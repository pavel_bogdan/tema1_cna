﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tema1_Service;

namespace Tema1_Service.Services
{
    public class PersonService : Person.PersonBase
    {
        private readonly ILogger<PersonService> _logger;
        public PersonService(ILogger<PersonService>logger)
        {
            _logger = logger;
        }

        private PersonResponse.Types.Gen GetGen(string cnp)
        {
            int firstChar = Convert.ToInt32(cnp[0]) % 2;
            if (firstChar == 1)
            {
                return PersonResponse.Types.Gen.M;
            }
            else
            {
                return PersonResponse.Types.Gen.F;
            }
        }

        private int GetAge(string cnp)
        {
            int year = 0;
            if (cnp[0] == '1' || cnp[0] == '2')
            {
                year = 1900 + Convert.ToInt32(cnp.Substring(1, 2));
            }

            if (cnp[0] == '5' || cnp[0] == '6')
            {
                year = 2000 + Convert.ToInt32(cnp.Substring(1, 2));
            }


            int month = Convert.ToInt32(cnp.Substring(3, 2));
            int day = Convert.ToInt32(cnp.Substring(5, 2));

            DateTime birthDate = new DateTime(year, month, day);

            DateTime now = DateTime.Now;
            TimeSpan span = now.Subtract(birthDate);

            return (new DateTime(1, 1, 1) + span).Year - 1;

        }

        public override Task<PersonResponse> GetPersonData(PersonData request, ServerCallContext context)
        {
            var person = request;

            string nume = person.Name;
            string cnp = person.Cnp;

            if(cnp.Length!=13)
            {
                _logger.Log(LogLevel.Information, "CNP invalid");
                return Task.FromResult(new PersonResponse());
            }

            PersonResponse.Types.Gen gen=GetGen(cnp);
            int age = GetAge(cnp);


            _logger.Log(LogLevel.Information, "Nume: " + nume);
            _logger.Log(LogLevel.Information, "CNP: " + cnp);
            _logger.Log(LogLevel.Information, "Varsta: " + age);
            _logger.Log(LogLevel.Information, "Sex: " + gen);
          

            return Task.FromResult(new PersonResponse() { Name = person.Name, Gen = gen, Age = age});
        }
    }
}
