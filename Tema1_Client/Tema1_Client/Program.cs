﻿using Grpc.Net.Client;
using System;
using System.Threading.Tasks;
using Tema1_Service;

namespace Tema1_Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Person.PersonClient(channel);


            Console.WriteLine("Introduceti numele:");
            string nume = Console.ReadLine();

            Console.WriteLine("Introduceti CNP-ul:");
            string cnp = Console.ReadLine();

            var person = new PersonData { Name = nume, Cnp = cnp };
            var response = await client.GetPersonDataAsync(person);

            Console.WriteLine(response);
        }
    }
}
